public class Decimalbinario {
 
    public void Bin(int num){
        int binario[] = new int[25];
        int index = 0;
        while(num > 0){
            binario[index++] = num%2;
            num = num/2;
        }
        for(int i = index-1;i >= 0;i--){
            System.out.print(binario[i]);
        }
    }
     
    public static void main(String a[]){
       Decimalbinario dtb = new Decimalbinario();
       //Aqui se introduce el numero a convertir.
        dtb.Bin(100);
    }
}